# -*- mode: ruby -*-
# vi: set ft=ruby :

# Require YAML module
require 'yaml'
 
# Read YAML file with box details
inventory = YAML.load_file('inventory/sib_hosts')

Vagrant.configure("2") do |cluster|

  ## Global settings for the cluster

  # disable synced folder for nodes
  # vagrant-vbguest need to disable even so
  cluster.vm.synced_folder ".", "/vagrant", disabled: true

  # if vagrant-vbguest plugin install don't try to install VBox additions
  if Vagrant.has_plugin?("vagrant-vbguest")
    cluster.vbguest.no_install = true
  end

  # use same private key on all machines
  cluster.ssh.insert_key = false
  
  # disable default rule
  cluster.vm.network "forwarded_port", guest: 22, host: 2222, id: "ssh", disabled: true
  
  # provisionners
  # sync timezone
  cluster.vm.provision "shell", inline: "sudo rm /etc/localtime && sudo ln -s /usr/share/zoneinfo/#{ENV['TZ'] || 'Europe/Paris' } /etc/localtime", run: "always"

  # copy vagrant public key to root authorized keys (to allow root access without password)
  cluster.vm.provision "ssh", type:"shell", inline: "sudo mkdir -p /root/.ssh && sudo cp /home/vagrant/.ssh/authorized_keys /root/.ssh/"

  # loop on host(s) in web group in inventory to create VM(s)
  inventory['all']['children']['web']['hosts'].each do |server,details|
    cluster.vm.define server do |srv|
      srv.vm.box = details['box']
      srv.vm.box_url = details['box_url']
      srv.vm.hostname = server
      srv.vm.network :private_network, ip: details['ansible_host']

      # set new rule for ssh
      srv.vm.network :forwarded_port, guest: 22, host: details['ssh_port'], host_ip: "127.0.0.1"
      
      srv.vm.provider "virtualbox" do |v|
        v.memory = details['memory']
        v.cpus = details['cpus']
      end
    end
  end
  # loop on host(s) in tower group in inventory to create VM(s)
  inventory['all']['children']['tower']['hosts'].each do |server,details|
    # start only tower box on demand
    cluster.vm.define server, autostart: false do |srv|
      srv.vm.box = details['box']
      srv.vm.box_url = details['box_url']
      srv.vm.hostname = server
      # disable because VM has already a network card on this subnet
      #srv.vm.network :private_network, ip: details['ansible_host']
      
      # set new rule for ssh
      srv.vm.network :forwarded_port, guest: 22, host: details['ssh_port'], host_ip: "127.0.0.1"

      srv.vm.provider "virtualbox" do |v|
        v.memory = details['memory']
        v.cpus = details['cpus']
      end
    end
  end
end
