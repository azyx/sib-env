# SIB-env

Environment for SIB training

## Schema

Made with [ASCIIFlow](http://asciiflow.com)

```
            Student computer                Virtual machines

            +-------------+               +------------------+
            |   Ansible   |               |      CentOS      |
            | controller  |  10.42.0.0/24 |   +----------+   |
            |             +---------------+   | node1-c7 |   |
            |  10.42.0.1  |      SSH      |   |          |   |
            |             |               |   |10.42.0.11|   |
            +-------------+               |   |          |   |
                                          |   |SSH port :|   |
              Working dirs                |   |   2211   |   +-----------------------+
                                          |   +----------+   |                       |
+---------------------------------+       |                  |     user: vagrant     |
|      Vagrant: $HOME/vagrant     |       |      Debian      |    with sudo rights   |
|      Ansible: $HOME/ansible     |       |   +----------+   |                       |
+---------------------------------+       |   | node2-deb|   +-----------------------+
                                          |   |          |   |
             Machine access               |   |10.42.0.12|   |
                                          |   |          |   |
+---------------------------------+       |   |SSH port :|   |
|      vagrant ssh node1-c7       |       |   |   2212   |   |
|      vagrant ssh node2-deb      |       |   +----------+   |
+---------------------------------+       |                  |
                                          +------------------+
                 DNS

+---------------------------------+
|  foo.fr, foofoo.fr : node1-c7   |
|                                 |
|  bar.fr, barbar.fr : node2-deb  |
|                                 |
+---------------------------------+

```

## Facilitator usage


## To prepare a SIB training (emulate a student machine in a SIB room)

Prerequisites on the facilitator machine:
 * Virtualbox installed
 * Vagrant installed
 * Ansible installed

```
mkdir $HOME/git
cd $HOME/git
git clone https://gitlab.com/azyx/sib-env.git
cd sib-env/facilitator
vagrant up
```

Vagrant will start three machines:
 1. controller-c7
 1. node1-c7
 1. node2-deb

The first one will be provision by Ansible with facilitator role and `lab` tag
in order to manage the two other machines.

On `controller-c7`, as `{{ local_user }}`, you can go in `ansible/` dir and run:
```
ansible web -i inventory/ -m ping
```

Still on the controller, you can use the [sib-draft repo](http://gitlab.com/azyx/sib-draft) to run
commands against the web group from `{{ local_user }}/ansible` dir. Example:

```
# in /home/usera/ansible
ansible-playbook ~/git/sib-draft/ansible_facts/ansible_facts.yml -i inventory/hosts
```

## To provision student machines in a SIB room

Prerequisites on the provisioner machine:
 * Ansible
 * Box files for VM in ~/vagrant/

1. Define an inventory file like this:

```
[students]
192.168.100.[1:20]
# facilitator
192.168.100.100

[students:vars]
ansible_user=root
ansible_ssh_pass=secret
```
2. Provision facilitator and students machine with:

```
ansible-playbook student.yml --tags sib_room -i PATH_TO_INVENTORY_FILE
```


## Student usage in a SIB room

Prerequisites:
 * Virtualbox installed
 * Machine provisioned

Commands to run:
``` shell
cd $HOME/vagrant
vagrant up
```
You will have two VM running: one with CentOS7 and another with Debian stable
running on your machine based on [inventory file](inventory/sib_hosts).
